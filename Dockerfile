FROM jboss/base-jdk:8

MAINTAINER ocutrin <ocutrin@setting.dev>

USER root

ENV WILDFLY_USER admin
ENV WILDFLY_PASSWORD admin

# Version de wildlfy
ENV WILDFLY_VERSION wildfly-10.1.0.Final

# Directorio de trabajo
WORKDIR /opt/jboss

# Directorio instalacion wildfly
ENV WILDFLY_DIR /opt/jboss/$WILDFLY_VERSION

# Añadimos el zip al contenedor
ADD $WILDFLY_VERSION.zip ./

# Instalacion wildfly
RUN unzip $WILDFLY_VERSION.zip && rm $WILDFLY_VERSION.zip

# Añadimos archivo de configuracion
ADD standalone.xml /$WILDFLY_DIR/standalone/configuration/

# Exponemos puertos que utilizaremos web 8080 admin 9990
EXPOSE 8080 9990

# Creamos password administrador
RUN $WILDFLY_DIR/bin/add-user.sh $WILDFLY_USER $WILDFLY_PASSWORD --silent

# Ejecutamos
CMD $WILDFLY_DIR/bin/standalone.sh -b 0.0.0.0 -bmanagement 0.0.0.0
