# README #

## WILDFLY ##

### Clonar repositorio ###
* git clone https://setting-ejemplo@bitbucket.org/setting-ejemplo/wildfly.git

### Crear imagen
* docker build -t settingejemplo/wildfly .

### Crear contenedor ###
* docker run \
  --detach \
  --restart always \
  --name wildfly-container \
  --network setting \
  --ip 172.13.1.30 \
  --publish 8080:8080 \
  --publish 9990:9990 \
  --link  mysql-container:mysql-db
  settingejemplo/wildfly
